def qw(lis):
    t = []
    for i in lis:
        t.append(i*i)
    return t

print(qw([1, 2, 3, 4, 5, 6, 7, 8, 9]))

def ch(lis):
    t = []
    for i in range(len(lis)):
        if i % 2 == 0:
            t.append(lis[i])
    return t

print(ch([1, 2, 3, 4, 5, 6, 7, 8, 9]))



def ku(lis):
    t = []
    for i in range(len(lis)):
        if i % 2 != 0 and lis[i] % 2 == 0:
            t.append(lis[i]**3)
    return t

print(ku([1, 2, 3, 4, 5, 6, 7, 8, 9]))

