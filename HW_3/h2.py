def union(*args,**kargs):
    t = []
    D = {}
    for seq in args: 
        t. append(seq)
    su = summ(t)
    mu = mult(t)
    D = kargs
    m = D.values()
    sus = summ(m)
    mus = mult(m)
    #print (su, sus, mu, mus)
    return su + sus, mu * mus
    


def summ(q):
    tot = 0
    for x in q: 
        if isinstance(x, (float, int)):
            tot += x 
        else:
            tot += summ(x) 
    return tot

def mult(q):
    tot = 1
    for x in q: 
        if isinstance(x, (float, int)):
            if x != 0:
                tot *= x 
        else:
            tot *= mult(x) 
    return tot

print(union(1, 2, [3, 4, (5, 6, 0)], a=(10, 11), b=(3, 4, [5, 6, [7, 8], []])))
