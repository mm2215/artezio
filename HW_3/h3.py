def ms_ot(a,b,c,d):
    s = (a + b + c + d)/4
    m = max(a, b, c, d)
    if ms_ot.data == 0:
        q = m
        ms_ot.data = q
    else:
        if m > ms_ot.data:
            q = m
        else:
            q = ms_ot.data
    ms_ot.data = q
    return s, q

ms_ot.data = 0

print(ms_ot(1, 2, 3, 4))
print(ms_ot(-3, -2, 10, 1))
print(ms_ot(7, 8, 8, 1))

